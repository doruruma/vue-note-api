<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Models\User;
use Illuminate\Support\Facades\Hash;

class AuthController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    // Sign In
    public function signIn(Request $req)
    {
        $validator = Validator::make($req->all(), [
            'username' => 'required|min:4',
            'password' => 'required|min:4'
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 422);
        }

        try {
            // Get user by username
            $user = User::where('username', $req->username)->first();
            if (!empty($user)) {
                // Validate Password
                if (Hash::check($req->password, $user->password)) {
                    return response()->json([
                        'title' => 'success',
                        'user_id' => $user->id,
                        'token' => $user->createToken('VueNoteToken')->accessToken,
                        'message' => 'Sign in Successfull'
                    ]);
                } else {
                    return response()->json([
                        'title' => 'error',
                        'message' => 'Invalid username or password'
                    ]);
                }
            } else {
                return response()->json([
                    'title' => 'error',
                    'message' => 'Invalid username or password'
                ]);
            }
        } catch (\Exception $e) {
            // Return Exception Message
            return response()->json($e->getMessage(), 500);
        }
    }

    // Create Account
    public function createAccount(Request $req)
    {
        $validator = Validator::make($req->all(), [
            'full_name' => 'required|min:4',
            'username' => 'required|min:4|unique:users,username',
            'password' => 'required|min:4'
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 422);
        }

        try {
            // Create new user
            $user = new User;
            $user->profile_image = 'default-profile.svg';
            $user->full_name = $req->full_name;
            $user->username = $req->username;
            $user->password = Hash::make($req->password);
            $user->save();

            // Return Response Data
            return response()->json([
                'title' => 'success',
                'message' => 'Account Created Successfully, You can now proceed to Sign In'
            ]);
        } catch (\Exception $e) {
            // Return Exception Message
            return response()->json($e->getMessage(), 500);
        }
    }
}