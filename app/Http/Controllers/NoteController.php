<?php

namespace App\Http\Controllers;

use App\Models\Note;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class NoteController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    // Get Notes
    public function getNotes(Request $req, $user_id)
    {
        try {
            // Get Data
            $notes = Note::where('user_id', $user_id)->orderBy('created_at', 'DESC')->get();

            // Return Response Data
            return response()->json($notes);
        } catch (\Exception $e) {
            // Return Exception Message
            return response()->json($e->getMessage(), 500);
        }
    }

    // Get Recent Notes
    public function getRecentNotes(Request $req, $user_id)
    {
        try {
            // Get Data
            $notes = Note::where('user_id', $user_id)->orderBy('id', 'DESC')->limit(3)->get();

            // Return Response Data
            return response()->json($notes);
        } catch (\Exception $e) {
            // Return Exception Message
            return response()->json($e->getMessage(), 500);
        }
    }

    // Get Detail Note
    public function detailNote(Request $req, $id)
    {
        try {
            // Get Data
            $note = Note::whereId($id)->first();

            // Return Response Data
            return response()->json($note);
        } catch (\Exception $e) {
            // Return Exception Message
            return response()->json($e->getMessage(), 500);
        }
    }

    // Create Note
    public function createNote(Request $req)
    {
        $validator = Validator::make($req->all(), [
            'user_id' => 'required|exists:users,id',
            'title' => 'required|min:3',
            'content' => 'required|min:4'
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 422);
        }

        try {
            // Create Data
            $note = new Note;
            $note->user_id = $req->user_id;
            $note->title = $req->title;
            $note->content = $req->content;
            $note->save();

            // Return Response Data
            return response()->json([
                'title' => 'success',
                'message' => 'Note Created Successfully'
            ]);
        } catch (\Exception $e) {
            // Return Exception Message
            return response()->json($e->getMessage(), 500);
        }
    }

    // Delete Notes
    public function deleteNote(Request $req, $id)
    {
        try {
            // Delete Data
            Note::whereId($id)->delete();

            // Return Response Data
            return response()->json([
                'title' => 'success',
                'message' => 'Notes Deleted Successfully'
            ]);
        } catch (\Exception $e) {
            // Return Exception Message
            return response()->json($e->getMessage(), 500);
        }
    }
}
