<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Rules\CheckOldPassword;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class ProfileController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    // Get Profile
    public function getProfile(Request $req, $id)
    {
        try {
            // Get Data
            $user = User::whereId($id)->first();

            // Return Response Data
            return response()->json($user);
        } catch (\Exception $e) {
            // Return Exception Message
            return response()->json($e->getMessage(), 500);
        }
    }

    // Change Password
    public function changePassword(Request $req, $id)
    {
        $validator = Validator::make($req->all(), [
            'old_password' => ['required', new CheckOldPassword($id)],
            'password' => 'required|min:4|different:old|confirmed',
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 422);
        }

        try {
            // Change Password
            $user = User::whereId($id)->first();
            $user->password = Hash::make($req->password);
            $user->save();

            // Return Response Data
            return response()->json([
                'title' => 'success',
                'message' => 'Password Changed'
            ]);
        } catch (\Exception $e) {
            // Return Exception Message
            return response()->json($e->getMessage(), 500);
        }
    }

    // Change Profile
    public function changeProfile(Request $req, $id)
    {
        $validator = Validator::make($req->all(), [
            'username' => 'required|min:3|unique:users,username,' . $id,
            'full_name' => 'required|min:4',
            'profile_image' => 'nullable|file|max:4096|mimes:jpeg,jpg,png'
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 422);
        }

        try {
            // Get User Data
            $user = User::whereId($id)->first();
            $imageName = $user->profile_image;
            
            // if Image Uploaded
            if ($req->hasFile('profile_image')) {
                // Delete Old Image
                if ($imageName != 'default-profile.svg') {
                    unlink(public_path('images/' . $imageName));
                }

                // Upload Image
                $image = $req->file('profile_image');
                $imageName =  strtotime(date('d-m-Y')) . Str::slug($req->full_name) . '.' . $image->getClientOriginalExtension();
                $image->move(public_path('images'), $imageName);                
            }

            // Update User Data
            $user->profile_image = $imageName;
            $user->username = $req->username;
            $user->full_name = $req->full_name;
            $user->save();

            // Return Response Data
            return response()->json([
                'title' => 'success',
                'message' => 'Profile Changed'
            ]);
        } catch (\Exception $e) {
            // Return Exception Message
            return response()->json($e->getMessage(), 500);
        }

    }
}
