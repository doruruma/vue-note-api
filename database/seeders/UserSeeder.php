<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Truncate Table
        User::truncate();

        // Seed Table
        $db = new User;
        $db->profile_image = 'default-profile.svg';
        $db->full_name = 'Rick Hunter';
        $db->username = 'rick_hunter';
        $db->password = Hash::make('rick_hunter');
        $db->save();

        $db = new User;
        $db->profile_image = 'default-profile.svg';
        $db->full_name = 'Alto Saotome';
        $db->username = 'alto_saotome';
        $db->password = Hash::make('alto_saotome');
        $db->save();
    }
}
