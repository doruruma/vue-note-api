<?php

namespace Database\Seeders;

use App\Models\Note;
use Illuminate\Database\Seeder;

class NoteSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Truncate Table
        Note::truncate();

        // Seed Table
        $db = new Note;
        $db->user_id = 1;
        $db->title = 'Rick first note';
        $db->content = 'Test Note By Rick Hunter';
        $db->save();

        $db = new Note;
        $db->user_id = 1;
        $db->title = 'Rick second note';
        $db->content = 'Test Note By Rick Hunter';
        $db->save();
        
        $db = new Note;
        $db->user_id = 1;
        $db->title = 'Rick third note';
        $db->content = 'Test Note By Rick Hunter';
        $db->save();

        $db = new Note;
        $db->user_id = 2;
        $db->title = 'Alto first note';
        $db->content = 'Test Note By Alto Saotome';
        $db->save();

        $db = new Note;
        $db->user_id = 2;
        $db->title = 'Alto second note';
        $db->content = 'Test Note By Alto Saotome';
        $db->save();

        $db = new Note;
        $db->user_id = 2;
        $db->title = 'Alto third note';
        $db->content = 'Test Note By Alto Saotome';
        $db->save();
    }
}
