<?php

/** @var \Laravel\Lumen\Routing\Router $router */

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});

/**
 * Authentication Route
 */
$router->group(['prefix' => 'authentication'], function () use ($router) {
    $router->post('/sign-in', 'AuthController@signIn');
    $router->post('/create-account', 'AuthController@createAccount');
});

/**
 * Passport Middleware
 */
$router->group(['middleware' => 'client'], function() use ($router) {

    /**
     * Notes Route
     */
    $router->group(['prefix' => 'notes'], function() use ($router) {
        $router->get('/get-recent-notes/{user_id}', 'NoteController@getRecentNotes');
        $router->get('/get-notes/{user_id}', 'NoteController@getNotes');
        $router->get('/detail/{id}', 'NoteController@detailNote');
        $router->post('/create-note', 'NoteController@createNote');
        $router->delete('/{id}', 'NoteController@deleteNote');
    });

    /**
     * Profile Route
     */
    $router->group(['prefix' => 'profile'], function() use ($router) {
        $router->get('/get-profile/{id}', 'ProfileController@getProfile');
        $router->put('/change-password/{id}', 'ProfileController@changePassword');
        $router->put('/change-profile/{id}', 'ProfileController@changeProfile');
    });

});
